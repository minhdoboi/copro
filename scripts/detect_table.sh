#!/bin/sh

PADDLE_DIR=~/work/autre/PaddleOCR

python $PADDLE_DIR/ppstructure/table/predict_table.py \
    --det_model_dir=$PADDLE_DIR/inference/en_ppocr_mobile_v2.0_table_det_infer \
    --rec_model_dir=$PADDLE_DIR/inference/en_ppocr_mobile_v2.0_table_rec_infer  \
    --table_model_dir=$PADDLE_DIR/inference/en_ppstructure_mobile_v2.0_SLANet_infer \
    --rec_char_dict_path=$PADDLE_DIR/ppocr/utils/dict/table_dict.txt \
    --table_char_dict_path=$PADDLE_DIR/ppocr/utils/dict/table_structure_dict.txt \
    --image_dir=data/output/releve12.jpg \
    --output=../output/table