from syndic.utils import dir_utils
from syndic.utils.extract_utils import *


def format_result(df: pd.DataFrame) -> pd.DataFrame:
    df = pd.concat([
        df,
        df.label.str.extract(r"(?i)(\d{2}/\d{2}/\d+) au (\d{2}/\d{2}/\d+)")
        .rename(columns={0: "start_date", 1: "end_date"})
    ], axis=1)
    df = df.assign(
        start_date=df.start_date.apply(parse_date),
        end_date=df.end_date.apply(parse_date)
    )
    return df


def get_releve_general():
    input_df = pd.read_csv(dir_utils.get_data_dir() + "/releve-general-depenses.csv")
    input_df["key"] = (input_df["key"] / 1000).astype(int)
    return format_result(input_df)
