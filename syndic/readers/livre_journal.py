from syndic.utils import dir_utils
from syndic.utils.extract_utils import *


def prepare(df):
    compte_extract = (
        df.compte.str.extract(r"^(\d+)[\r\n](.*)")
        .rename(columns={0: "compte_id", 1: "compte_name"})
        .astype({"compte_id": "Int64"})
    )
    contrepartie_extract = (
        df.contrepartie.str.extract(r"^(\d+)[\r\n](.*)")
        .rename(columns={0: "contrepartie_id", 1: "contrepartie_name"})
        .astype({"contrepartie_id": "Int64"})
    )

    interval_extract = extract_interval(df.detail.str)
    df = pd.concat([df,
                    compte_extract,
                    contrepartie_extract,
                    interval_extract
                    ], axis=1).drop(columns=["compte", "contrepartie"])

    return df


def read_df():
    return prepare(pd.read_csv(dir_utils.get_data_dir() + "/livre-journal.csv"))
