import pandas as pd
from . import livre_journal, releve_general, grand_livre
from ..utils import dir_utils


def read_grand_livre():
    return grand_livre.read_df()


def read_cumuls():
    return pd.read_csv(dir_utils.get_data_dir() + "/grand-livre-cumuls.csv")


def read_releves():
    return pd.read_csv(dir_utils.get_root_dir() + "/data/0213/releve_bancaire.csv")


def read_releve_general():
    return releve_general.get_releve_general()


def read_livre_journal():
    return livre_journal.read_df()
