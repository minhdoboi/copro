import pandas as pd

from syndic.utils import dir_utils
from syndic.readers import livre_journal


def prepare(df: pd.DataFrame) -> pd.DataFrame:
    return livre_journal.prepare(df)


def read_df():
    return prepare(pd.read_csv(dir_utils.get_data_dir() + "/livre-journal.csv"))
