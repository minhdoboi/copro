from PIL.Image import Image

from syndic.prep.model import Area

_PAGE_INCHES = (8.267717, 11.69291)
_TABULA_SIZE = (_PAGE_INCHES[0] * 72, _PAGE_INCHES[1] * 72)


class PageArea:

    def __init__(self, page, image: Image):
        self.image = image
        self.page = page
        self.is_portrait = image.size[0] < image.size[1]
        self.ratio = self.get_ratio()

    def search_for(self, value: str):
        return [self.convert_area(pos) for pos in self.page.search_for(value)]

    def convert_area(self, img_area: Area):
        if self.is_portrait:
            return img_area
        else:
            bound = self.page.bound()
            return [
                (bound[2] - img_area[3]),
                img_area[0],
                (bound[2] - img_area[1]),
                img_area[2]
            ]

    def get_ratio(self):
        img_size = self.image.size if self.is_portrait else (self.image.size[1], self.image.size[0])
        return img_size[0] / _TABULA_SIZE[0], img_size[1] / _TABULA_SIZE[1]

    def get_area(self, img_area: Area):
        return self.image.crop((
            int(img_area[0] * self.ratio[1]),
            int(img_area[1] * self.ratio[0]),
            int(img_area[2] * self.ratio[1]),
            int(img_area[3] * self.ratio[0]),
        ))
