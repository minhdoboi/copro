import pandas as pd
from typing import Any, List, Dict

from syndic.prep.model import Line, Bloc, BlocInterval, BlocDelim, Area
from syndic.prep.page_area import PageArea
from syndic.prep.specs import LineContextExtractor, LineSpecs, BlocIntervalDef, DataDef, DataSpecs, NextRowMergeSpecs
from syndic.utils import dir_utils
import re

from syndic.utils.extract_utils import parse_float, parse_date

path = dir_utils.get_data_dir() + "/Releve-general-depenses.pdf"
key_pattern = re.compile('(?P<code>\d{8}) (?P<name>.*)$')
charge_pattern = re.compile('(?P<name>Charges.*) (?P<code>\d{3})$')  # bug


def get_line_specs():
    return LineSpecs(
        line_start_index=13,
        line_end_index=-1,
        ctx_extractor=RGContextExtractor()
    )


def get_bloc_interval_def():
    return RGBlocIntervalDef()


def get_data_specs():
    merge_specs = NextRowMergeSpecs(
        ref_column="date",
        merge_columns=["label", "supplier"]
    )

    return DataSpecs(
        data_def=RGDataDef(),
        next_row_merge_specs=merge_specs
    )


def is_key(value: str):
    return key_pattern.match(value) is not None


def get_charge(value: str):
    m = charge_pattern.match(value)
    return (m.group("code"), m.group("name")) if m else None


class RGContextExtractor(LineContextExtractor):

    def get_next_context(self, line: Line) -> Dict[str, Any]:
        context = {}

        if is_key(line.content):
            context["key"] = line.content
            line.infos["is_key"] = True
        else:
            charge = get_charge(line.content)
            if charge:
                context["charge"] = charge
                line.infos["is_charge"] = True
        return context


class RGBlocIntervalDef(BlocIntervalDef):

    def get_bloc_intervals(self, lines: List[Line], page_area: PageArea) -> List[BlocInterval]:
        lines_filtered = [
            line for line in lines if not line.infos.get("is_charge")
                                      and not line.content.startswith("Total")]
        sous_total_pos = page_area.search_for("Sous total")

        bloc_interval = BlocInterval()
        results = []
        cur = 0
        cur_sous_total = 0
        for line in lines_filtered:
            if line.infos.get("is_key"):  # start bloc
                pos = page_area.search_for(line.content)
                bloc_interval.start = BlocDelim(line, pos[0])  # key is supposed unique
                cur = 0
            elif line.content.startswith("Sous total"):  # end bloc
                pos = sous_total_pos[cur_sous_total]
                bloc_interval.end = BlocDelim(line, pos)
                cur = 1
                cur_sous_total += 1
                results.append(bloc_interval)
                bloc_interval = BlocInterval()
        if cur == 0:  # start but no end
            results.append(bloc_interval)
        return results

    def get_limit_pos(self, page_area: PageArea) -> Area:
        pos_charges_du = page_area.search_for("Charges du")[0]
        pos_imprime_le = page_area.search_for("Imprimé le")[0]
        pos_montant = page_area.search_for("Montant TTC")[0]
        left = pos_charges_du[0] - 5
        top = pos_charges_du[3]
        right = pos_montant[2] + 5
        bottom = pos_imprime_le[1]
        return left, top, right, bottom


class RGDataDef(DataDef):

    def format_df(self, df: pd.DataFrame, bloc: Bloc):
        df.columns = ["label", "supplier", "date", "tva", "charge_amount", "amount"]
        m = key_pattern.match(bloc.lines[0].infos["key"])
        df["key"] = int(m.group("code"))
        df["key_label"] = m.group("name")
        code_charge, charge = bloc.lines[0].infos["charge"]
        df["charge_code"] = int(code_charge)
        df["charge_name"] = charge
        df["date"] = df.date.apply(parse_date)
        df["tva"] = parse_float(df["tva"])
        df["charge_amount"] = parse_float(df["charge_amount"])
        df["amount"] = parse_float(df["amount"])
        return df
