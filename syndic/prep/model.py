from typing import Dict, Any, List, Tuple, Optional
from dataclasses import dataclass, field
from PIL.Image import Image
from fitz import Document


@dataclass
class Line:
    row_num: int
    content: str
    infos: Dict[str, Any] = field(default_factory=lambda: {})


@dataclass
class PageLines:
    num_page: int
    lines: List[Line]


@dataclass
class DocLines:
    path: str
    doc: Document
    pages_lines: List[PageLines]


Area = Tuple[int, int, int, int]

@dataclass
class BlocDelim:
    line: Line
    pos: Area


@dataclass
class BlocInterval:
    start: Optional[BlocDelim] = None
    end: Optional[BlocDelim] = None



@dataclass
class BlocImage:
    image: Image
    num_page: int
    area: Area


@dataclass
class Bloc:
    lines: List[Line] = field(default_factory=lambda: [])
    images: List[BlocImage] = field(default_factory=lambda: [])
