import pandas as pd
from typing import Any, List, Dict

from syndic.prep.model import Line, Bloc, BlocInterval, BlocDelim, Area
from syndic.prep.page_area import PageArea
from syndic.prep.specs import LineContextExtractor, LineSpecs, BlocIntervalDef, DataDef, DataSpecs
from syndic.utils import dir_utils
from syndic.utils.extract_utils import parse_float, parse_date

path = dir_utils.get_data_dir() + "/RechercheEcritureJournaux.pdf"


def get_line_specs():
    return LineSpecs(
        ctx_extractor=LivreJournalContextExtractor(),
        first_page_start_index=5,
        line_start_index = 1,
        line_end_index = -1
    )


def get_bloc_interval_def():
    return LiveJournalBlocIntervalDef()


def get_data_specs():
    return DataSpecs(
        data_def = LiveJournalDataDef(),
        lattice=True
    )


class LivreJournalContextExtractor(LineContextExtractor):
    def get_next_context(self, line: Line) -> Dict[str, Any]:
        if line.content.startswith("Journal de"):
            current_journal = line.content
            line.infos["is_journal"] = True
            return {"journal": current_journal}
        else:
            return {}


class LiveJournalBlocIntervalDef(BlocIntervalDef):

    def get_bloc_intervals(self, lines: List[Line], page_area: PageArea) -> List[BlocInterval]:
        pc_pos = page_area.search_for("pièces comptables")
        bloc_interval = BlocInterval()
        results = []
        cur = 0
        cur_pc = 0
        for line in lines:
            if line.infos.get("is_journal"):  # start bloc
                pos = page_area.search_for(line.content)[0]
                bloc_interval.start = BlocDelim(line, pos)
                cur = 0
            elif "pièces comptables" in line.content:  # end bloc
                pos = pc_pos[cur_pc]
                bloc_interval.end = BlocDelim(line, pos)
                cur = 1
                cur_pc += 1
                # remove last bloc
                if not(len(results) > 0 and bloc_interval.start is None):
                    results.append(bloc_interval)
                bloc_interval = BlocInterval()
        if cur == 0:  # start but no end
            results.append(bloc_interval)
        return results

    def get_limit_pos(self, page_area: PageArea) -> Area:
        pos_nb = page_area.search_for("Nb")[0]
        pos_imprime_le = page_area.search_for("Sous-total")[0]
        pos_credit = page_area.search_for(" Crédit")[0]
        left = pos_nb[0] - 10
        top = pos_nb[2]
        right = pos_credit[2] + 30
        bottom = pos_imprime_le[1]
        return left, top, right, bottom


class LiveJournalDataDef(DataDef):

    def format_df(self, df: pd.DataFrame, bloc: Bloc):
        df.columns = ["nb", "num_ecriture", "date", "compte", "detail",
                      "contrepartie", "rapproche", "debit", "credit"]
        df["journal"] = bloc.lines[0].infos["journal"]
        df["date"]=df["date"].apply(parse_date)
        df["rapproche"] = df.rapproche == "OUI"
        df["debit"] = parse_float(df["debit"])
        df["credit"] = parse_float(df["credit"])
        return df
