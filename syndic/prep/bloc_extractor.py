from pdf2image import convert_from_path

from syndic.prep.page_area import PageArea
from syndic.prep.specs import BlocIntervalDef
from syndic.prep.model import *


def is_not_empty_area(img_area: Area):
    _, t, _, b = img_area
    return abs(t - b) > 10


class BlocExtractor:

    def __init__(self,
                 doc_lines: DocLines,
                 bloc_interval_def: BlocIntervalDef
                 ):
        self.path = doc_lines.path
        self.images = convert_from_path(doc_lines.path)
        self.bloc_interval_def = bloc_interval_def
        self.doc = doc_lines.doc
        self.page_bloc_extractors = [
            self.PageBlocExtractor(self, page_line)
            for page_line in doc_lines.pages_lines
        ]

    def read_blocs(self):
        blocs = []
        current_bloc = None
        for page_ext in self.page_bloc_extractors:
            blocs, current_bloc = page_ext.compute_blocs(blocs, current_bloc)
        return blocs

    class PageBlocExtractor:

        def __init__(self, outer, page_lines: PageLines):
            self.outer: BlocExtractor = outer
            self.bloc_interval_def = outer.bloc_interval_def
            self.num_page = page_lines.num_page
            self.lines = page_lines.lines
            self.page = outer.doc[self.num_page]
            self.image = self.outer.images[self.num_page]
            self.page_area = PageArea(self.page, self.image)

        def compute_blocs(self, blocs, current_bloc):
            """ Determine the blocs from img blocs. The method should be called in a loop
            supplying the current_bloc of the iteration that can comes from a previous page.
                * images are for debugging
                * (numpage, area) are needed to extract datas
                * lines contains additional infos
            """
            img_blocs = self.get_img_blocs()
            start_row_num = self.lines[0].row_num
            end_row_num = self.lines[-1].row_num
            offset = start_row_num
            for img, area, interval in img_blocs:
                if interval.start is None and current_bloc is not None:
                    # will attach the img_bloc to the current bloc
                    bloc = current_bloc
                else:
                    bloc = Bloc()
                    current_bloc = bloc
                    blocs.append(bloc)
                if img is not None:
                    bloc.images.append(BlocImage(img, self.num_page, area))
                line_start = offset if interval.start is None else interval.start.line.row_num
                line_end = end_row_num if interval.end is None else interval.end.line.row_num
                offset = line_end + 1
                bloc.lines = [line for line in self.lines if line_start <= line.row_num <= line_end]
            return blocs, current_bloc

        def get_img_blocs(self) -> List[Tuple[Optional[Image], Area, BlocInterval]]:
            left, top, right, bottom = self.bloc_interval_def.get_limit_pos(self.page_area)
            bloc_intervals = self.bloc_interval_def.get_bloc_intervals(self.lines, self.page_area)

            img_blocs = []
            for interval in bloc_intervals:
                t = top if interval.start is None else interval.start.pos[3]
                b = bottom if interval.end is None else interval.end.pos[1]
                img_area = (left, t, right, b)
                img = self.page_area.get_area(img_area) if is_not_empty_area(img_area) else None
                img_blocs.append((img, img_area, interval))
            return img_blocs
