from typing import List, Optional

import pandas as pd
import tabula

from syndic.prep.data_row_merge import merge_next_row, merge_row_around
from syndic.prep.model import Bloc
from syndic.prep.specs import DataSpecs


class DataExtractor:

    def __init__(self, path: str, data_specs: DataSpecs):
        self.path = path
        self.data_specs = data_specs

    def read_datas(self, blocs: List[Bloc]):
        dfs = [self.read_bloc_data(bloc) for bloc in blocs]
        return pd.concat([df for df in dfs if df is not None], ignore_index=True)

    def read_bloc_data(self, bloc) -> Optional[pd.DataFrame]:
        """ Read the data of a bloc, that may come from multiple images on different pages
        """
        dfs = []
        for bloc_image in bloc.images:
            parsed_dfs = self.read_data(bloc_image.area, bloc_image.num_page)
            if parsed_dfs:
                df = parsed_dfs[0]
                try:
                    df = self.data_specs.data_def.format_df(df, bloc)
                except Exception as e:
                    raise Exception(f"Failed formatting {bloc}", e)
                dfs.append(df)
        if not dfs:
            return None
        result = pd.concat(dfs, ignore_index=True)
        if self.data_specs.next_row_merge_specs is not None:
            result = merge_next_row(result, self.data_specs.next_row_merge_specs)
        elif self.data_specs.row_around_merge_specs is not None:
            result = merge_row_around(result, self.data_specs.row_around_merge_specs)
        return result

    def read_data(self, img_area, num_page) -> List[pd.DataFrame]:
        """ Read an isolated data from a picture of a bloc
        """
        area = [img_area[1], img_area[0], img_area[3], img_area[2]]
        return tabula.read_pdf(self.path,
                               lattice=self.data_specs.lattice,
                               area=area,
                               pandas_options={'header': None},
                               columns=self.data_specs.columns_pos,
                               pages=num_page + 1)
