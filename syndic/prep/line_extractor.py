from copy import copy

import fitz
from pypdf import PdfReader
from .specs import *


class LineExtractor:

    def __init__(self, path: str, specs: LineSpecs):
        self.path = path
        self.pdf = PdfReader(str(path))
        self.doc = fitz.open(path)
        self.specs = specs
        self.page_line_extractors = [
            self.PageLineExtractor(self, num_page)
            for num_page in range(len(self.doc))
        ]

    def compute(self):
        current_line_context = {}
        for page_ext in self.page_line_extractors:
            current_line_context = page_ext.compute_infos(current_line_context)

        return DocLines(
            path=self.path,
            doc=self.doc,
            pages_lines=[
                PageLines(
                    num_page=page_line_ext.num_page,
                    lines=page_line_ext.lines)
                for page_line_ext in self.page_line_extractors
            ]
        )

    class PageLineExtractor:

        def __init__(self, outer, num_page: int):
            self.outer: LineExtractor = outer
            self.specs = outer.specs
            self.num_page = num_page
            self.lines = self.get_lines()

        def get_lines(self):
            raw_lines = self.outer.pdf.pages[self.num_page].extract_text().split("\n")
            lines = []
            for line in raw_lines:
                if any(p for p in self.specs.ignored_patterns if p in line):
                    continue
                lines.append(line)
            return [
                Line(row_num, content) for row_num, content in enumerate(lines)
                if (self.specs.first_page_start_index is None or (
                            self.num_page != 0 or row_num >= self.specs.first_page_start_index))
                   and (self.specs.line_start_index is None or row_num >= self.specs.line_start_index)
                   and (self.specs.line_end_index is None or row_num < len(lines) + self.specs.line_end_index)
            ]

        def compute_infos(self, current_line_context: Dict[str, Any]):
            for line in self.lines:
                ctx = self.outer.specs.ctx_extractor.get_next_context(line)
                if ctx:
                    current_line_context = copy(current_line_context)
                    for k, v in ctx.items():
                        current_line_context[k] = v
                for k, v in current_line_context.items():
                    line.infos[k] = v
            return current_line_context
