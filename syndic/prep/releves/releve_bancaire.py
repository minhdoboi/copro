import tabula
import fitz

from syndic.prep.releves import releve_bancaire_ocr
from syndic.common import *
from syndic.utils import dir_utils
from syndic.utils.extract_utils import parse_float


def get_rb_path(date):
    return dir_utils.get_root_dir() + f"/data/0213/Extrait+de+comptes+Compte+10674+000202961+01+COMPTE+SEPARE+SYNDICAT+COPRO+SDC+VILLA+SALOME+au+{date}.pdf"


def read_data(rb_path):
    doc = fitz.open(rb_path)
    dfs=[]
    for page_number , page in enumerate(doc):
        # check there's some data
        date_valeur_rect = page.search_for("Date Valeur")
        if date_valeur_rect:
            date_valeur_rect = date_valeur_rect[0]
            area = [date_valeur_rect[0], 0, 750, 600]
            iban_rect = page.search_for("IBAN")
            if iban_rect:
                area[2]=iban_rect[0][3]
            dfs.extend(tabula.read_pdf(rb_path, lattice=True, pages=page_number+1, area=area))
    return pd.concat(dfs, ignore_index=True) if dfs else None


def clean(df):
    df = df.rename(columns={
        "Date": "date",
        "Date valeur": "date_valeur",
        "Opération": "operation",
        "Débit EUROS": "debit",
        "Crédit EUROS": "credit"
    })
    df_str=df.astype({"date": str})
    df = df.loc[df.date.isnull() | ~(df_str.date.str.contains("SOLDE") | df_str.date.str.contains("Total"))]
    df["date"] = pd.to_datetime(df["date"], format="%d/%m/%Y")
    df["date_valeur"] = pd.to_datetime(df["date_valeur"], format="%d/%m/%Y")
    return group_operation(add_op_id(df)).assign(
        debit=parse_float(df.debit.astype("string")),
        credit=parse_float(df.credit.astype("string")))

def add_op_id(df):
    df = df.assign(op_id=df.index.where(df.date.notnull(), np.nan))
    df.op_id = df.op_id.ffill()
    return df

def group_operation(df):
    def get_detail(ops):
        return list(ops[1:])

    return (
        df
        .join(df.groupby("op_id").operation.apply(get_detail).rename("detail"))
        .query("date.notnull()")
        .drop(columns="op_id")
    )

DATES=[
        "2022-01-31",
        "2022-02-28",
        "2022-03-31",
        "2022-04-29",
        "2022-05-31",
        "2022-06-30",
        "2022-07-29",
        "2022-08-31",
        "2022-09-30",
        "2022-10-31",
        "2022-11-30",
    ]

def build_all():
    def read_df(date):
        # print(f"read {date}")
        data = read_data(get_rb_path(date))
        if data is None:
            print(f"{date} is empty")
        return clean(data) if data is not None else None

    dfs=[ read_df(date) for date in DATES]
    for df_ocr in releve_bancaire_ocr.read_all():
        dfs.append(clean(df_ocr))

    df= pd.concat(dfs, ignore_index=True)
    df.to_csv(dir_utils.get_root_dir() + "/data/releve_bancaire.csv", index=False)
    return df
