from pdf2image import convert_from_path
import os
import pandas as pd
from paddleocr import PaddleOCR

from syndic.utils import dir_utils

PATHS = [dir_utils.get_root_dir() + "/data/0213/2190.pdf"]
output_dir = dir_utils.get_root_dir() + "/data/output"


def prepare_image(path):
    images = convert_from_path(path)

    os.makedirs(output_dir, exist_ok=True)

    img = images[0]
    img = img.crop((100, 950, img.size[0], 1770))
    imgpath = os.path.splitext(path)[0] + ".jpg"
    print(f"save to {imgpath}")
    img.save(imgpath)
    return imgpath, img


def analyze(imgpath):
    ocr = PaddleOCR(use_angle_cls=False, use_space_char=True,
                    lang='french')  # need to run only once to download and load model into memory
    return ocr.ocr(imgpath, cls=False)


def to_df(raw_data):
    sources = raw_data[0]
    headers, rows = build_rows(sources)
    datas = build_columns(rows, headers)
    return pd.DataFrame(datas)


#
# build the table manually
#

def build_rows(sources):
    rows = []
    row_data = []
    headers = []
    row_cursor = None
    for source in sources:
        source_pos = source[0]
        if row_cursor is None:
            row_cursor = source_pos
        else:
            row_mean = (source_pos[2][1] + source_pos[0][1]) / 2
            if row_mean > row_cursor[2][1]:
                # change row
                row_cursor = source_pos
                if not headers:
                    headers = row_data
                else:
                    rows.append(row_data)
                row_data = []
        row_data.append(source)
    rows.append(row_data)
    return headers, rows


def build_columns(rows, headers):
    datas = []
    for row in rows:
        row_data = {}
        datas.append(row_data)
        for cell in row:
            cell_header = None
            for header in headers:
                if cell[0][1][0] < header[0][0][0]:
                    break
                cell_header = header
            row_data[cell_header[1][0]] = cell[1][0]
    return datas


def build_all():
    for path in PATHS:
        imgpath, img = prepare_image(path)
        raw_datas = analyze(imgpath)
        output_path = os.path.splitext(path)[0] + ".csv"
        print(f"Build {output_path}")
        to_df(raw_datas).to_csv(output_path, index=False)


def read_all():
    datas = []
    for path in PATHS:
        data_path = os.path.splitext(path)[0] + ".csv"
        datas.append(pd.read_csv(data_path))
    return datas
