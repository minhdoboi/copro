from typing import List
import functools
import pandas as pd
import numpy as np
from syndic.prep.specs import NextRowMergeSpecs, RowAroundMergeSpecs


def merge_next_row(df: pd.DataFrame, merge_specs: NextRowMergeSpecs):
    cols = df.columns
    df["id"] = df.index.where(
        df[merge_specs.ref_column].notnull(), np.nan)
    df["id"] = df.id.ffill()

    return merge_rows(df, cols, merge_specs.merge_columns)


def merge_row_around(df: pd.DataFrame, merge_specs: RowAroundMergeSpecs) -> pd.DataFrame:
    cols = df.columns
    ids = list((
                       functools.reduce(lambda a, b: a | b
                                        , [df[col].isnull() for col in merge_specs.merge_columns]) |
                       df[merge_specs.ref_column].notnull()).astype(int) * df.index)
    for idx, id in enumerate(ids):
        if id == 0 and len(ids) > idx + 2 and ids[idx + 1] != 0 and ids[idx + 2] == 0:
            ids[idx] = ids[idx + 1]
            ids[idx + 2] = ids[idx + 1]
    df["id"] = pd.Series(ids)
    result = merge_rows(df, cols, merge_specs.merge_columns)
    if "id" in result.columns:
       result = result.drop(columns="id")
    return result


def join_str(s: pd.Series):
    return " ".join(s.dropna())


def merge_rows(df: pd.DataFrame, cols: List[str], merge_columns: List[str]):
    aggs = {
        column: join_str
        for column in merge_columns
    }

    for col in cols:
        if col not in aggs:
            aggs[col] = "first"

    df = df.groupby("id").agg(aggs)
    return df
