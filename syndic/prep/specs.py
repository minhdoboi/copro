import pandas as pd
from .model import *
from abc import ABC
from typing import Any, Optional, Iterable
from dataclasses import dataclass

from .page_area import PageArea


class LineContextExtractor(ABC):
    def get_next_context(self, line: Line) -> Dict[str, Any]:
        return {}


@dataclass
class LineSpecs:
    ctx_extractor: LineContextExtractor
    ignored_patterns: List[str] = field(default_factory=lambda: [])
    first_page_start_index: Optional[int] = None
    line_start_index: Optional[int] = None
    line_end_index: Optional[int] = None


class BlocIntervalDef(ABC):

    def get_bloc_intervals(self, lines: List[Line], page_area: PageArea) -> List[BlocInterval]:
        pass

    def get_limit_pos(self, page_area: PageArea) -> Area:
        pass


@dataclass
class NextRowMergeSpecs:
    ref_column: str
    merge_columns: List[str]


@dataclass
class RowAroundMergeSpecs:
    ref_column: str
    merge_columns: List[str]


class DataDef(ABC):

    def format_df(self, df: pd.DataFrame, bloc: Bloc):
        return df


@dataclass
class DataSpecs:
    data_def: DataDef
    next_row_merge_specs: Optional[NextRowMergeSpecs] = None
    row_around_merge_specs: Optional[RowAroundMergeSpecs] = None
    lattice: bool = False
    columns_pos: Optional[Iterable[float]] = None
