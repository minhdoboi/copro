import pandas as pd
import numpy as np
from typing import Any, List, Dict
import re

from syndic.prep.model import Line, Bloc, BlocInterval, BlocDelim, Area, PageLines
from syndic.prep.page_area import PageArea
from syndic.prep.specs import LineContextExtractor, LineSpecs, BlocIntervalDef, DataDef, DataSpecs, RowAroundMergeSpecs
from syndic.utils import dir_utils
from syndic.utils.extract_utils import parse_float, parse_date

path = dir_utils.get_data_dir() + "/Grand-Livre-Date.pdf"
classe_pattern = re.compile('^(?P<code>\d{2,3}) (?P<name>[\w\' ]+)$')
compte_pattern = re.compile('^Compte (?P<code>\d+) +\: +(?P<label>[\w\' ]+) Cumul.*$')


def get_line_specs():
    return LineSpecs(
        ctx_extractor=JournalContextExtractor(),
        ignored_patterns=[
            "Total Copropriété",
            "Total sur la période",
            "Total Classe",
            "Résidence Villa Salomé",
            "Imprimé le"],
        first_page_start_index=7,
        line_start_index=3,
        # line_end_index=-1
    )


def get_bloc_interval_def():
    return GLBlocIntervalDef()


def get_data_specs():
    row_around_merge_specs = RowAroundMergeSpecs(
        ref_column="date",
        merge_columns=["label"]
    )

    return DataSpecs(
        data_def=GLDataDef(),
        columns_pos=[54, 95, 130, 270, 320, 390, 445, 520],
        row_around_merge_specs=row_around_merge_specs
    )


def get_cumuls(pages_lines: List[PageLines]):
    cumul_pattern = re.compile('Compte (?P<code>\d+).*Cumul au \d{2}/\d{2}/\d{4} (?P<amount>[\d, ]+)$')

    cumul_lines = [
        {"code": m.group("code"), "amount": m.group("amount")}
        for pl in pages_lines
        for m in [cumul_pattern.match(line.content) for line in pl.lines if "Cumul" in line.content]
        if m is not None
    ]
    df = pd.DataFrame(cumul_lines)
    df["amount"] = parse_float(df["amount"])
    return df.astype({"code": int})


def get_classe(value: str):
    if value.strip() == "479":
        return "479", ""
    else:
        m = classe_pattern.match(value)
        return (m.group("code"), m.group("name")) if m else None


def get_compte(value: str):
    m = compte_pattern.match(value)
    return (m.group("code"), m.group("label")) if m else None


class JournalContextExtractor(LineContextExtractor):
    def get_next_context(self, line: Line) -> Dict[str, Any]:
        context = {}

        classe = get_classe(line.content)
        if classe:
            context["classe"] = classe
            line.infos["is_classe"] = True
        else:
            compte = get_compte(line.content)
            if compte:
                context["compte"] = compte
                line.infos["is_compte"] = True
        return context


class GLBlocIntervalDef(BlocIntervalDef):

    def get_bloc_intervals(self, lines: List[Line], page_area: PageArea) -> List[BlocInterval]:
        lines_filtered = [line for line in lines if not line.infos.get("is_classe")]
        total_pos = page_area.search_for("Total Compte")

        bloc_interval = BlocInterval()
        results = []
        cur = 0
        cur_total = 0
        for line in lines_filtered:
            if line.infos.get("is_compte"):  # start bloc
                pos = page_area.search_for(line.content)
                bloc_interval.start = BlocDelim(line, pos[0])
                cur = 0
            elif "Total Compte" in line.content:  # end bloc
                pos = total_pos[cur_total]
                bloc_interval.end = BlocDelim(line, pos)
                cur = 1
                cur_total += 1
                results.append(bloc_interval)
                bloc_interval = BlocInterval()
        if cur == 0:  # start but no end
            results.append(bloc_interval)
        return results

    def get_limit_pos(self, page_area: PageArea) -> Area:
        pos_charges_du = page_area.search_for("Journal")[0]
        pos_imprime_le = page_area.search_for("Imprimé le")[0]
        pos_montant = page_area.search_for("Solde créditeur")[0]
        left = pos_charges_du[0] - 5
        top = pos_charges_du[3] + 5
        right = pos_montant[2] + 5
        bottom = pos_imprime_le[1]
        return left, top, right, bottom


class GLDataDef(DataDef):

    def format_df(self, df: pd.DataFrame, bloc: Bloc):
        df = fill_columns(df, 9)
        df.columns = ["journal", "date", "contrepartie", "label", "num_piece", "debit", "credit", "solde_debit",
                      "solde_credit"]
        df["date"] = df["date"].apply(parse_date)
        df["debit"] = parse_float(df["debit"])
        df["credit"] = parse_float(df["credit"])
        df["solde_debit"] = parse_float(df["solde_debit"])
        df["solde_credit"] = parse_float(df["solde_credit"])
        df["classe"] = bloc.lines[0].infos["classe"][0]
        df["classe_label"] = bloc.lines[0].infos["classe"][1]
        df["compte"] = bloc.lines[0].infos["compte"][0]
        df["compte_label"] = bloc.lines[0].infos["compte"][1]
        return df


def fill_columns(df: pd.DataFrame, target_size) -> pd.DataFrame:
    for i in range(len(df.columns), target_size):
        df[i] = np.nan
    return df
