import os
import syndic


def get_root_dir() -> str:
    return os.path.normpath(os.path.dirname(syndic.__file__) + "/../")


def get_data_dir() -> str:
    return get_root_dir()+"/data/0524"
