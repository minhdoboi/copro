import pandas as pd


def parse_date(date):
    if isinstance(date, str):
        if len(date) == 10:
            return pd.to_datetime(date, format="%d/%m/%Y")
        elif len(date) == 8:
            return pd.to_datetime(date, format="%d/%m/%y")
    return pd.NaT


def parse_float(value: pd.Series) -> pd.Series:
    if value.dtype != "object":
        return value
    else:
        return (
            value.str.replace(".", "", regex=False)
            .str.replace(",", ".", regex=False)
            .str.replace(";", ".", regex=False)
            .str.replace(" ", "", regex=False).astype("Float64")
        )


def extract_interval(value: pd.Series) -> pd.DataFrame:
    df = (
        value
        .extract(r"(?i)(\d{2}/\d{2}/\d+) au (\d{2}/\d{2}/\d+)")
        .rename(columns={0: "start_date", 1: "end_date"})
    )
    return df.assign(
        start_date=df.start_date.apply(parse_date),
        end_date=df.end_date.apply(parse_date)
    )
