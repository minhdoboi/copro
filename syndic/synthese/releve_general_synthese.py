import pandas as pd
import datetime
from syndic.constants import *

def rev(d):
    return {x: k for k, v in d.items() for x in v}


def get_cat_gestion(x):
    charge_key = x["key"]
    cat_cent = charge_key // 100
    cat_mille = charge_key // 1000

    cat_mapping = rev({
        CAT_NETT: [61401]
    })
    cent_mapping = rev({
        CAT_TIERS: [623, 624],
        CAT_CONTRAT_ENT: [614],
        CAT_EAU: [601],
        CAT_ELEC: [602],
        CAT_ASS: [616],
        CAT_ENT_REP: [615],
        CAT_AUT: [662],
    })
    mille_mapping = rev({
        CAT_SYNDIC: [62],
    })

    return (
            cat_mapping.get(charge_key)
            or cent_mapping.get(cat_cent)
            or mille_mapping.get(cat_mille)
            or pd.NA
    )


def get_synthese(rg):
    return rg.assign(gestion_category=lambda df: df.apply(get_cat_gestion, axis=1))


def find_missing_dates(df, year):
    df = df.sort_values(by="start_date")
    first_day = pd.to_datetime(datetime.date(year, 1, 1))
    last_day = pd.to_datetime(datetime.date(year, 12, 31))

    df = df.assign(
        start_date=df.start_date.where(df.start_date.isnull() | (df.start_date > first_day), first_day),
        end_date=df.end_date.where(df.end_date.isnull() | (df.end_date < last_day), last_day))
    days = df.apply(lambda x: pd.date_range(x.start_date, x.end_date).to_series(), axis=1).columns
    missing_days = pd.concat([pd.date_range(first_day, last_day).to_series(), days.to_series()]).drop_duplicates(
        keep=False)
    return missing_days.diff().dt.days.ne(1).cumsum().reset_index().rename(columns={0: "id", "index": "date"}).groupby(
        "id").agg(
        start=("date", "min"),
        end=("date", "max")
    )

