import pandas as pd
import numpy as np
import re
from pandas.api.types import is_numeric_dtype


def get_evolution(x, y):
    return np.round(100 * (y - x) / x, 2)


def add_total(df, columns=None, styled=True):
    if isinstance(df.index, pd.MultiIndex):
        key = [""] * df.index.nlevels
        key[-1] = 'Total'
        key = tuple(key)
    else:
        key = 'Total'
    s = df.sum(numeric_only=True)
    df = df.copy()
    df.loc[key, :] = s[columns] if columns else s

    if styled and columns:
        df = df.style.format(
            "",
            subset=(
                key,
                [col for col in df.columns if col not in columns],
            ),
        )
    return df


def add_evolution(df):
    def get_groups(col):
        matches = re.match(".*(\d{4})", col)
        if matches:
            return matches.groups()
        else:
            return []

    years = sorted(list({year for col in df.columns for year in get_groups(col)}), reverse=True)
    if len(years) > 1:
        after = years[0]
        before = years[1]
        for col in df.columns:
            strb = str(before)
            if col.endswith(strb) and is_numeric_dtype(df[col]):
                prefix = col[:-len(strb)]
                cola = f"{prefix}{after}"
                if cola in df.columns:
                    df.loc[:, f"{prefix}evolution"] = get_evolution(df[col], df[cola])
    return df


def enrich_result(df):
    return add_evolution(add_total(df))


def get_compte_journal(journal: pd.DataFrame, compte_id, report=False, args={}, total=False):
    df = journal.query(f"compte_id=={compte_id} or contrepartie_id=={compte_id}")
    if not report:
        df = df.query("not detail.str.contains('Report')")
    for k, v in args.items():
        col_type = df.dtypes[k].type
        if issubclass(col_type, str) or issubclass(col_type, object) or issubclass(col_type, np.datetime64):
            df = df.query(f"{k} == '{v}'")
        else:
            df = df.query(f"{k} == {v}")
    if total:
        return add_total(df, columns=["debit", "credit"])
    else:
        return df


def get_comptes(journal):
    return journal[["compte_id", "compte"]].drop_duplicates().set_index("compte_id")
