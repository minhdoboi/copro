import pandas as pd
from syndic.constants import *

synthese_2019 = pd.DataFrame.from_records([
    (CAT_TIERS,6510.28, {623, 624}),
    (CAT_NETT ,5774.52, {611}),
    (CAT_CONTRAT_ENT,4973.26,  {614, 606}),
    (CAT_SYNDIC,4070.76, {621, 622}),
    (CAT_EAU,3003.21, {601}),
    (CAT_ASS,1731.91, {616}),
    (CAT_ELEC,2057.06, {602}),
    (CAT_ENT_REP,903.10, {615}),
    (CAT_AUT,681.28, {662}),
], columns = ["gestion_category", "synthese_2019", "key_2019"]).set_index("gestion_category")


entretien_2019 = pd.DataFrame.from_records([
    (61408 , 149.74),
    (61411 , 452.44),
    (61419 , 312.37),
    (61400 , 178.2),
    (61410 , 206.4),
    (61520 , 922.24),
    # (?? ,  174.98), # location compteurs EF
    (61404 , 2153.52),
], columns = ["key", "montant_2019"]).set_index("key")

frais_gestion_2019 = pd.DataFrame.from_records([
    (60600 , 26.76),
    (62000 , 2675.98),
    (62100 , 136.52),
    (66200 , 105-63),
], columns = ["key", "montant_2019"]).set_index("key")

tiers_interv_2019 = pd.DataFrame.from_records([
    (62300 , "expert+avocat", 1452+4119.28),
    (62700 , "Rémunérations de tiers intervenants", 800),
    (62800 , "Frais du conseil syndical", 139+234+30),
], columns = ["key", "key_label", "montant_2019"]).set_index(["key", "key_label"])


assur_2019 = pd.DataFrame.from_records([
    (61600 , 1612.421),
    (61610 , 119.49),
], columns = ["key", "montant_2019"]).set_index(["key"])

ent_2019 = pd.DataFrame.from_records([
    (615 , "réparation portail", 903.1),
], columns = ["key", "key_label", "montant_2019"]).set_index(["key", "key_label"])